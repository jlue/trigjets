#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include<string>



void gfex_SC(){
     int runs[4]=             {801925,  801926,   801931,   801932};
     string LambdaDarks[4]=   {"1.6",   "1.6",     "40",     "40"};
     int ctaus[4]=            {1,       10,       1,        50};
     int colors[4] =          {1,       2,        3,        4};

     TCanvas *JetOnAll = new TCanvas("JetOnAll","Jet turn-ons: All",200,10,700,500);   
     TCanvas *JetOnJ100 = new TCanvas("JetOnJ100","Jet turn-ons: J100",200,10,700,500);    
     TCanvas *JetOngLJ100 = new TCanvas("JetOngLJ100","Jet turn-ons: J100",200,10,700,500);    
     TCanvas *JetOngLJ140 = new TCanvas("JetOngLJ140","Jet turn-ons: J100",200,10,700,500);   
     TCanvas *JetOnSC111 = new TCanvas("JetOnSC111","Jet turn-ons: SC111",200,10,700,500); 
     TCanvas *JetOnSC111j = new TCanvas("JetOngC111","Jet turn-ons: SC111j",200,10,700,500);     
     TLegend *legAll = new TLegend(0.50, 0.10 ,0.90,0.45); 
     TLegend *legJ100 = new TLegend(0.50, 0.10 ,0.90,0.45); 
     TLegend *leggLJ100 = new TLegend(0.50, 0.10 ,0.90,0.45); 
     TLegend *leggLJ140 = new TLegend(0.50, 0.10 ,0.90,0.45); 
     TLegend *legSC111 = new TLegend(0.50, 0.10 ,0.90,0.45); 
     TLegend *legSC111j = new TLegend(0.50, 0.10 ,0.90,0.45); 

     for (int i = 0; i<sizeof(runs)/sizeof(int);i++){
          int run = runs[i];
          string LambdaDark = LambdaDarks[i];
          int ctau = ctaus[i];
          cout << run << endl; 
           gStyle->SetOptStat(0);
          char inputFile[80];
          snprintf(inputFile,80,"data/MyxAODAnalysis.%d.outputs.root", run);
          char lj100[80];
          snprintf(lj100,80,"j100: %d(#LambdaD:%s c#tau:%d)", run, LambdaDark.c_str(), ctau);
          char lgLJ100[80];
          snprintf(lgLJ100,80,"gLJ100: %d(#LambdaD:%s c#tau:%d)", run, LambdaDark.c_str(), ctau);
          char lgLJ140[80];
          snprintf(lgLJ140,80,"gLJ140: %d(#LambdaD:%s c#tau:%d)", run, LambdaDark.c_str(), ctau);
          char lSC111[80];
          snprintf(lSC111,80,"SC111: %d(#LambdaD:%s c#tau:%d)", run, LambdaDark.c_str(), ctau);
          char lSC111j[80];
          snprintf(lSC111j,80,"SC111j: %d(#LambdaD:%s c#tau:%d)", run, LambdaDark.c_str(), ctau);
          TFile *ejf = new TFile(inputFile);
          TH1F *h_hltLjetPt; 
          ejf->GetObject("h_hltLjetPt",h_hltLjetPt);

          TH1F *h_hltLjetPt_J100;
          ejf->GetObject("h_hltLjetPt_J100", h_hltLjetPt_J100 );

          TH1F *h_hltLjetPt_gLJ100;	
          ejf->GetObject("h_hltLjetPt_gLJ100", h_hltLjetPt_gLJ100 );

          TH1F *h_hltLjetPt_gLJ140;
          ejf->GetObject("h_hltLjetPt_gLJ140", h_hltLjetPt_gLJ140 );

          TH1F *h_hltLjetPt_SC111;
          ejf->GetObject("h_hltLjetPt_SC111", h_hltLjetPt_SC111 );

          TH1F *h_hltLjetPt_SC111j;
          ejf->GetObject("h_hltLjetPt_SC111j", h_hltLjetPt_SC111j );

          TH1F *h_hltLjetE_J100  = (TH1F *) h_hltLjetPt->Clone("h_hltLjetE_J100");
          h_hltLjetE_J100->Divide(h_hltLjetPt_J100, h_hltLjetPt ,1., 1., "B");

          TH1F *h_hltLjetE_gLJ100  = (TH1F *) h_hltLjetPt->Clone("h_hltLjetE_gLJ100");
          h_hltLjetE_gLJ100->Divide(h_hltLjetPt_gLJ100, h_hltLjetPt ,1., 1., "B");
          
          TH1F *h_hltLjetE_gLJ140  = (TH1F *) h_hltLjetPt->Clone("h_hltLjetE_gLJ140");
          h_hltLjetE_gLJ140->Divide(h_hltLjetPt_gLJ140, h_hltLjetPt ,1., 1., "B");

          TH1F *h_hltLjetE_SC111  = (TH1F *) h_hltLjetPt->Clone("h_hltLjetE_SC111");
          h_hltLjetE_SC111->Divide(h_hltLjetPt_SC111, h_hltLjetPt ,1., 1., "B");

          TH1F *h_hltLjetE_SC111j  = (TH1F *) h_hltLjetPt->Clone("h_hltLjetE_SC111j");
          h_hltLjetE_SC111j->Divide(h_hltLjetPt_SC111j, h_hltLjetPt ,1., 1., "B");


          JetOnSC111->cd();
          JetOnSC111->Draw();

          h_hltLjetE_SC111->SetMarkerColor (colors[i]);
          h_hltLjetE_SC111->SetTitle("Jet Turn-Ons for SC111");
          h_hltLjetE_SC111->SetMarkerStyle(24); 
          h_hltLjetE_SC111->Draw("Same");
                              

          legSC111->AddEntry(h_hltLjetE_SC111, lSC111 ,"P");
          legSC111->Draw();

          //SC111j Plot: 

          JetOnSC111j->cd();
          JetOnSC111j->Draw();

          h_hltLjetE_SC111j->SetMarkerColor (colors[i]);
          h_hltLjetE_SC111j->SetTitle("Jet Turn-Ons for SC111j");
          h_hltLjetE_SC111j->SetMarkerStyle(24); 
          h_hltLjetE_SC111j->Draw("Same");
                              

          legSC111j->AddEntry(h_hltLjetE_SC111j, lSC111j ,"P");
          legSC111j->Draw(); 


          //J100 Plot:


          JetOnJ100->cd();
          JetOnJ100->Draw();
          h_hltLjetE_J100->SetMarkerColor (colors[i]);
          h_hltLjetE_J100->SetTitle("Jet Turn-Ons for J100."); 
          h_hltLjetE_J100->SetMarkerStyle(24); 
          h_hltLjetE_J100->Draw("Same");
          
          legJ100->AddEntry(h_hltLjetE_J100,lj100,"P");
          legJ100->Draw(); 

          // h_hltLjetE_J100->SetTitle("Jet Turn-Ons for J100, gLJ100, gLJ140");
 //Why does this line have to be here??


          //gLJ100 Plot:
          JetOngLJ100->cd();
          JetOngLJ100->Draw();
          h_hltLjetE_gLJ100->SetMarkerColor (colors[i]);
          h_hltLjetE_gLJ100->SetTitle("Jet Turn-Ons for gLJ100."); 
          h_hltLjetE_gLJ100->SetMarkerStyle(20); 
          h_hltLjetE_gLJ100->Draw("Same");
          
          leggLJ100->AddEntry(h_hltLjetE_gLJ100,lgLJ100,"P");
          leggLJ100->Draw(); 

          //gLJ140 Plot:
          JetOngLJ140->cd();
          JetOngLJ140->Draw();
          h_hltLjetE_gLJ140->SetMarkerColor (colors[i]);
          h_hltLjetE_gLJ140->SetTitle("Jet Turn-Ons for gLJ140."); 
          h_hltLjetE_gLJ140->SetMarkerStyle(21); 
          h_hltLjetE_gLJ140->Draw("Same");
          
          leggLJ140->AddEntry(h_hltLjetE_gLJ140,lgLJ140,"P");
          leggLJ140->Draw(); 
     }



     //Create a pdf of select plots.
     JetOnSC111->Print("JetOnAllwSC111j.pdf(","pdf");
     JetOnSC111j->Print("JetOnAllwSC111j.pdf","pdf");
     JetOnJ100->Print("JetOnAllwSC111j.pdf","pdf");
     JetOngLJ100->Print("JetOnAllwSC111j.pdf","pdf");
     JetOngLJ140->Print("JetOnAllwSC111j.pdf)","pdf");


     return ;

   
     
}
