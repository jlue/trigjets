#include <AsgMessaging/MessageCheck.h>
#include <TrigJetsAnalysis/TrigJetsxAODAnalysis.h>

#include <TH1.h>
#include <string.h>                                                                                                                      
#include <vector>  

#include <xAODJet/JetContainer.h>
#include <TrigDecisionTool/TrigDecisionTool.h>
#include <xAODTruth/TruthParticleContainer.h>
#include <xAODTruth/TruthParticle.h>
#include <xAODEventInfo/EventInfo.h>

//#include <xAODCaloEvent/CaloClusterContainer.h>

#include <xAODMissingET/MissingETContainer.h>

#include <xAODTrigger/JetRoIContainer.h>
#include <xAODTrigger/JetRoIAuxContainer.h>
#include <xAODTrigger/JetRoIContainer.h>
#include <xAODTrigger/JetRoI.h>
#include <xAODTrigger/jFexSRJetRoIContainer.h>
#include <xAODTrigger/jFexLRJetRoIContainer.h>
#include <xAODTrigger/gFexJetRoIContainer.h>


TrigJetsxAODAnalysis :: TrigJetsxAODAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm ( name, pSvcLocator),    
      m_trigDecTool( "TrigDecisionTool/tdt", this )
      // m_vTrigChainNames({})
{
  declareProperty( "triggerDecTool", m_trigDecTool, "the trigger decision tool");
  ////Edit the following in the share/JobOptions.py files.////
  declareProperty( "triggerStrings", p_triggerStrings[0] = {"L1_gLJ80p0ETA25", "L1_gJ100p0ETA25", "HLT_j460_a10r_L1J100" }, "A list of the triggers and trigger chains");
  declareProperty( "jetContainers", p_jetContainers[0] = {"HLT_AntiKt10EMPFlowCSSKSoftDropBeta100Zcut10Jets_jes_ftf" }, "A list of the jet containers");    
  declareProperty( "cutChains", p_cutChains[0] = {"NoCuts" }, "A list of the jet containers");    
}


StatusCode TrigJetsxAODAnalysis :: initialize ()
{
  ////  Retrieve the Trigger Decision--Possibly the hardest part! //// 
  ANA_CHECK( m_trigDecTool.retrieve());
  if ( !m_trigDecTool.empty() ) {
      ANA_MSG_DEBUG( "TDT retrieved" );
  }else{
    ANA_MSG_WARNING( "Failed to retreive TDT.");
  }

  ANA_MSG_INFO ("Initializing with jetContainers = " << p_jetContainers[0] << ".");
  ANA_MSG_INFO ("Initializing with triggerStrings = " << p_triggerStrings[0] << ".");
  


  m_NjetContainers=p_jetContainers->size();
  m_NtriggerStrings=p_triggerStrings->size();
  m_NcutChains=p_cutChains->size();
  ANA_MSG_INFO( "Making histograms for " << m_NtriggerStrings << " trigger strings, " << m_NjetContainers << " jet containers, and " << m_NcutChains << " cut chains.");
    ////TODO: Add trigger chain functionality.
    // // If the trigger chain is specified, parse it into a list.
    // if ( m_triggerChainString!="" ) {
    //   StatusCode sc = parseList(m_triggerChainString,m_vTrigChainNames);
    //   // print outs like this cause trouble for AnalysisBase
    //   ANA_MSG_DEBUG("DMS in parse list"<<m_triggerChainString);
    //   ANA_MSG_DEBUG("DMS out parse list"<<m_vTrigChainNames);
    //   if ( !sc.isSuccess() ) {
    //     ANA_MSG_WARNING("Error parsing trigger chain list, using empty list instead.");
    //     m_vTrigChainNames.clear();
    //   }      
    // }
                                                                        
  ////TODO: Put all the histograms in a tree.
  ANA_CHECK (book (TTree ("analysis", "TrigJets analysis ntuple")));
  TTree* mytree = tree ("analysis");
  mytree->Branch ("EventNumber", &m_eventNumber);

  ////Book Histograms for HLT/reco jet pT////
  std::string hTitle="";
  for (auto &jetContainerName: p_jetContainers[0]){
    for (auto &triggerString: p_triggerStrings[0]){
      for (auto &cutChain: p_cutChains[0]){
        for (auto &truthString: m_truthStrings){
          for (auto &varString: m_varStrings){
            hName = m_strh + triggerString + m_str_ + jetContainerName + m_str_ + cutChain + truthString + varString;
            hTitle= m_hTitlePrefix + triggerString + m_hTruthTitleSuffix1 + varString + truthString + m_hTruthTitleSuffix2;
            // ANA_MSG_INFO ( "Making histogram: " << hName << ", Title: " <<hTitle);
            // ANA_MSG_INFO ( "stringcmp: " << varString.c_str() << " and " <<m_strPT.c_str() << " gives " << !strcmp(varString.c_str(), m_strPT.c_str()));
            if (!strcmp(varString.c_str(), m_strPT.c_str())){
              ANA_CHECK (book (TH1F (hName.c_str(), hTitle.c_str(), 100, 0,500)));
            }else{
              ANA_CHECK (book (TH1F (hName.c_str(), hTitle.c_str(), 100, 0,4))); 
            }
          }
        }
      }
    }
  }

  // ////Book Histograms for truth jet pT (inferred from truth particles)////
  // for (int j=0; j<m_NjetContainers;j++){
  //   for (int t=0; t<m_NtriggerStrings;t++){
  //     for (int c=0; c<m_NcutChains; c++){
  //       m_strCuts="NoEtaCut_vsTruth";
  //       hName = m_strh + p_triggerStrings[0][t] + m_str_ + p_jetContainers[0][j] + m_str2+m_strCuts;
  //       m_hdEtaName = m_strh + p_triggerStrings[0][t] + m_str_ + p_jetContainers[0][j] + m_str2 + m_strCuts + m_strDEta;
  //       m_hdPhiName = m_strh + p_triggerStrings[0][t] + m_str_ + p_jetContainers[0][j] + m_str2 + m_strCuts + m_strDPhi;
  //       m_hRName = m_strh + p_triggerStrings[0][t] + m_str_ + p_jetContainers[0][j] + m_str2 + m_strCuts + m_strR;
  //       hTitle= m_hTitlePrefix + p_triggerStrings[0][t] + m_hTruthTitleSuffix;
  //       ANA_CHECK (book (TH1F (hName.c_str(), hTitle.c_str(), 100, 0,500)));
  //       ANA_CHECK (book (TH1F (m_hdEtaName.c_str(), hTitle.c_str(), 100, 0,4)));
  //       ANA_CHECK (book (TH1F (m_hdPhiName.c_str(), hTitle.c_str(), 100, 0,4)));
  //       ANA_CHECK (book (TH1F (m_hRName.c_str(), hTitle.c_str(), 100, 0,4)));
  //       m_strCuts="1p8EtaCut_vsTruth";
  //       hName = m_strh + p_triggerStrings[0][t] + m_str_ + p_jetContainers[0][j] + m_str2+m_strCuts;
  //       hTitle= m_hTitlePrefix + p_triggerStrings[0][t] + m_hTruthTitleSuffix;
  //       ANA_CHECK (book (TH1F (hName.c_str(), hTitle.c_str(), 100, 0,500)));
  //       m_strCuts="pT200Cut_1p8EtaCut_vsTruth";
  //       hName = m_strh + p_triggerStrings[0][t] + m_str_ + p_jetContainers[0][j] + m_str2+m_strCuts;
  //       hTitle= m_hTitlePrefix + p_triggerStrings[0][t] + m_hTruthTitleSuffix;
  //       ANA_CHECK (book (TH1F (hName.c_str(), hTitle.c_str(), 100, 0,500)));
  //     }
  //   }
  // }
  return StatusCode::SUCCESS;
}

StatusCode TrigJetsxAODAnalysis :: execute ()
{

  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));
  ANA_MSG_DEBUG ("in execute, runNumber = " << eventInfo->runNumber() <<", eventNumber = " << eventInfo->eventNumber() );

  //// Loop over jet containers to get the pt/eta/phi of leading jet. ////
  for (auto &jetContainerName: p_jetContainers[0]){

    //// Truth jets: ////
    std::vector< const xAOD::TruthParticle* > MCTruthList;
    const xAOD::TruthParticleContainer* truthParticles = 0;
    ANA_CHECK (evtStore()->retrieve (truthParticles, "TruthParticles")); 
    if( !evtStore()->retrieve( truthParticles, "TruthParticles").isSuccess() ){
      Error("excute()", "Failed to retrieve TruthParticles info. Exiting." );
      return StatusCode::FAILURE;
    }
    ////Declare truth particle vars:////
    ////TODO: Put these in the tree////
    int HardProcessQuarksCount = 0;
    float truthParticlePt = 0;
    float truthParticleEta = 0;
    float truthParticlePhi = 0;
    float subtruthParticlePt = 0;
    float subtruthParticleEta = 0;
    float deltaTruthParticleEta = 0;
    float subtruthParticlePhi = 0;
    float deltaTruthParticlePhi = 0;
    float deltaRTruthParticle = 0;
    int DownQuarkCount = 0;
    //// Get the pt/eta/phi of the leading of the two dark quarks(pdgId=+/-4900101) or down quarks (pdgId=+/-1) coming out of the hard process(status=23).////
    for( const auto truth_particle: *truthParticles ) {
      int truthStatus = truth_particle->status();
      ////Get particles exiting the hard process:////
      if( truthStatus == 23){
        ////Check that they are either dark quarks or down quarks:
        int pdgId = truth_particle->pdgId();
        if (abs(pdgId) != 4900101){
          if (abs(pdgId) == 1){
            DownQuarkCount++;
          }else{
          ANA_MSG_WARNING("WARNING: Unexpected pdgID for particle with status=23: PdgId = " << pdgId);
            Error("excute()", "Unexpected pdgID. Exiting." );
            return StatusCode::FAILURE; 
          }
        }
        ////Get the stats of the leading and subleading truth particles:
        if (truth_particle->pt() > subtruthParticlePt){
          if (truth_particle->pt() > truthParticlePt){
            subtruthParticlePt = truthParticlePt;
            subtruthParticleEta = truthParticleEta;
            subtruthParticlePhi = truthParticlePhi;
            truthParticlePt = truth_particle->pt();
            truthParticleEta = truth_particle->eta();
            truthParticlePhi = truth_particle->phi();   
          }else{
            subtruthParticlePt = truth_particle->pt();
            subtruthParticleEta = truth_particle->eta();
            subtruthParticlePhi = truth_particle->phi();
          }
        }
        ////In theory, there should be only two particles exiting the hard process:
        if (HardProcessQuarksCount ==1){
          deltaTruthParticleEta = abs(truthParticleEta-subtruthParticleEta);
          deltaTruthParticlePhi = abs(truthParticlePhi-subtruthParticlePhi);
          deltaRTruthParticle= std::sqrt(deltaTruthParticleEta*deltaTruthParticleEta+deltaTruthParticlePhi*deltaTruthParticlePhi);
          // std::cout<<"DeltaEta: "<< deltaTruthParticleEta << " \t  DeltaPhi: " << deltaTruthParticlePhi<<std::endl;
          break;
        }   
        HardProcessQuarksCount++;
      }
    }
    // ANA_MSG_INFO("DownQuark Count: " << HardProcessQuarksCount);

    ////Declare jet vars.////
    float leadJetPt=0;
    float leadJetEta=-1000;
    float leadJetPhi=-1000;
    float subleadJetPt=0;
    float subleadJetEta=-1000;
    float subleadJetPhi=-1000;
    //// Get the pt/eta/phi of the leading and subleading HLT/reco jets:
    if ( jetContainerName == "TruthJets"){
      leadJetPt = truthParticlePt;
      leadJetEta = truthParticleEta;
      leadJetPhi = truthParticlePhi;
      subleadJetPt = subtruthParticlePt;
      subleadJetEta = subtruthParticleEta;
      subleadJetPhi = subtruthParticlePhi;
    }else{
      const xAOD::JetContainer* jets = nullptr;  
      ANA_CHECK (evtStore()->retrieve (jets,jetContainerName));
        
      // std::cout<< "Test : " << jets->GetBinContent(1)->pt() <<std::end;
      for (const xAOD::Jet *jet : *jets) {
        if (jet->pt() > subleadJetPt){
          if (jet->pt() > leadJetPt){
            subleadJetPt = leadJetPt;
            subleadJetEta = leadJetEta;
            subleadJetPhi = leadJetPhi;
            leadJetPt = jet->pt();
            leadJetEta = jet->eta();
            leadJetPhi = jet->phi();
          }else{
            subleadJetPt = jet->pt();
            subleadJetEta = jet->eta();
            subleadJetPhi = jet->phi();
          }
        }
        // ANA_MSG_INFO ("execute(): first jet pt = " << (jet->pt() * 0.001) << " GeV, phi " << (jet->phi()) << ", eta=" << (jet->eta()) );
      }
    }
    // ANA_MSG_INFO("Lead pt: "<<leadJetPt <<"**************");

    // Check the trigger decision. ////
    for (auto &triggerString: p_triggerStrings[0]){
      m_trigDecision = m_trigDecTool->isPassed(triggerString);

      ////  Fill the histograms. ////
      std::string strVetoTrigDec="VetoTrigDec";
      //Pass if trig passes:
      if (m_trigDecision == 1 || !strcmp(triggerString.c_str(), strVetoTrigDec.c_str())){
        ////For now, cutChain entries must be selected from the following:"////
        for (auto &cutChain: p_cutChains[0]){
          std::string m_str200Cut_1p8EtaCut = "pT200Cut_1p8EtaCut";
          std::string m_strp1p8EtaCut = "1p8EtaCut";
          std::string m_strnoCuts = "noCuts";
          ////Do not write unless the cutChain conditions are met:
          if( !( !strcmp(cutChain.c_str(),m_strnoCuts.c_str()) || (!strcmp(cutChain.c_str(),m_strp1p8EtaCut.c_str()) && (abs(truthParticleEta)<1.8) && (abs(subtruthParticleEta)<1.8)) ||  (!strcmp(cutChain.c_str(),m_str200Cut_1p8EtaCut.c_str()) && (abs(truthParticleEta)<1.8) && (abs(subtruthParticleEta)<1.8) && (leadJetPt*0.001>200) ) ) ){
            break;
          }else{             
            for (auto &varString: m_varStrings){
              for (auto &truthString: m_truthStrings){
                ////Filling histograms with truth variables:////
                hName = m_strh + triggerString + m_str_ + jetContainerName + m_str_ + cutChain + truthString + varString;
                // ANA_MSG_INFO("hname: " <<hName);
                if (!strcmp(truthString.c_str(), m_strTruth.c_str())){
                  ////TODO: Some of the following is redundant and doesn't need to be done again.////
                  float pT = truthParticlePt;
                  float eta = truthParticleEta;
                  float phi = truthParticlePhi;
                  float dEta = deltaTruthParticleEta;
                  float dPhi = deltaTruthParticlePhi;
                  float dR = deltaRTruthParticle;
                  if (!strcmp(varString.c_str(), m_strPT.c_str())){
                    hist (hName.c_str())->Fill (pT*0.001);
                  }else if(!strcmp(varString.c_str(), m_strEta.c_str())){
                    hist (hName.c_str())->Fill (eta);
                  }else if(!strcmp(varString.c_str(), m_strPhi.c_str())){
                    hist (hName.c_str())->Fill (phi);
                  }else if(!strcmp(varString.c_str(), m_strDEta.c_str())){
                    hist (hName.c_str())->Fill (dEta);
                  }else if(!strcmp(varString.c_str(), m_strDPhi.c_str())){
                    hist (hName.c_str())->Fill (dPhi);
                  }else if(!strcmp(varString.c_str(), m_strDR.c_str())){
                    hist (hName.c_str())->Fill (dR);
                  }else {
                    ANA_MSG_WARNING("You should never reach this line. It means that you've requested a var that is not handled for truth particles.");
                  }
                  
                ////Filling histograms with jet container vars:////
                }else if (!strcmp(truthString.c_str(), m_strNull.c_str())){
                  float pT = leadJetPt;
                  float eta = leadJetEta;
                  float phi = leadJetPhi;
                  float dEta = abs(leadJetEta-subleadJetEta);
                  float dPhi = abs(leadJetPhi-subleadJetPhi);
                  float dR = std::sqrt(dEta*dEta+dPhi*dPhi);
                  if (!strcmp(varString.c_str(), m_strPT.c_str())){
                    hist (hName.c_str())->Fill (pT*0.001);
                  }else if(!strcmp(varString.c_str(), m_strEta.c_str())){
                    hist (hName.c_str())->Fill (eta);
                  }else if(!strcmp(varString.c_str(), m_strPhi.c_str())){
                    hist (hName.c_str())->Fill (phi);
                  }else if(!strcmp(varString.c_str(), m_strDEta.c_str())){
                    hist (hName.c_str())->Fill (dEta);
                  }else if(!strcmp(varString.c_str(), m_strDPhi.c_str())){
                    hist (hName.c_str())->Fill (dPhi);
                  }else if(!strcmp(varString.c_str(), m_strDR.c_str())){
                    hist (hName.c_str())->Fill (dR);
                  }else {
                    ANA_MSG_WARNING("You should never reach this line. It means that you've requested a var that is not handled in the jet container.");
                  }
                }
              }
            }
          }
        }
      }
    } 
  }

  tree ("analysis")->Fill ();
  return StatusCode::SUCCESS;
}


StatusCode TrigJetsxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk. This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  return StatusCode::SUCCESS;
}



// StatusCode TrigJetsxAODAnalysis::parseList(const std::string& line, std::vector<std::string>& result) const {
//   //
//   // Copied from AthMonitorAlgorithm - parse the supplied trigger chains
//   //
//   std::string item;
//   std::stringstream ss(line);

//   ANA_MSG_DEBUG( "L1CaloAODAnalysis::parseList()" );

//   while ( std::getline(ss, item, ',') ) {
//     std::stringstream iss(item); // remove whitespace
//     iss >> item;
//     result.push_back(item);
//   }

//   return StatusCode::SUCCESS;
// }


