# See: https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/SoftwareTutorialxAODAnalysisInCMake for more details about anything here

# Specify local input file name
#testFile = os.getenv('ASG_TEST_FILE_MC')
testFile = '/eos/home-s/strom/ej/AOD.37358759._000001.pool.root.1'

# Override next line on command line with: --filesInput=XXX
jps.AthenaCommonFlags.FilesInput = [testFile] 

# Specify AccessMode (read mode) ... ClassAccess is good default for xAOD
jps.AthenaCommonFlags.AccessMode = "ClassAccess" 

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'TrigJetsxAODAnalysis', 'AnalysisAlg' )

# Later on we'll add some configuration options for our algorithm that go here

from AnaAlgorithm.DualUseConfig import addPrivateTool
# add the TDT tool to the algorithm
addPrivateTool( alg, 'triggerDecTool', 'Trig::TrigDecisionTool' )
# alg.TriggerChain="L1_gLJ140,L1_gJ100"

#alg.triggerStrings = ['L1_J100', 'L1_jXE60', 'L1_jJ160', 'L1_gXEJWOJ100', 'L1_gJ100p0ETA25', 'L1_gLJ80p0ETA25', 'L1_gLJ100p0ETA25', 'L1_gLJ140p0ETA25', 'L1_gLJ160p0ETA25', 'L1_SC111-CjJ40', 'L1_SC111-CJ15', 'HLT_j460_a10r_L1J100', 'HLT_j200_0eta180_emergingPTF0p08dR1p2_a10sd_cssk_pf_jes_ftf_preselj200_L1J100', 'HLT_j200_0eta160_emergingPTF0p08dR1p2_a10sd_cssk_pf_jes_ftf_preselj200_L1J100', 'HLT_j200_0eta160_emergingPTF0p08dR1p2_a10sd_cssk_pf_jes_ftf_preselj200_L1jJ160', 'HLT_j200_0eta180_emergingPTF0p08dR1p2_a10sd_cssk_pf_jes_ftf_preselj200_L1jJ160', 'HLT_j180_hitdvjet260_medium_L1J100', 'HLT_j180_hitdvjet260_medium_L1jJ160', 'HLT_j180_hitdvjet260_tight_L1jJ160', 'HLT_j180_2dispjet50_3d2p_L1J100', 'HLT_j180_2dispjet50_3d2p_L1J100', '', 'HLT_j420_35smcINF_a10sd_cssk_pf_jes_ftf_preselj225_L1gLJ140p0ETA25', 'HLT_j460_a10_lcw_subjes_L1gLJ140p0ETA25', 'HLT_2j330_35smcINF_a10t_lcw_jes_L1gLJ140p0ETA25', 'HLT_j420_35smcINF_a10t_lcw_jes_L1gLJ140p0ETA25']


# alg.triggerStrings = ['L1_J100', 'L1_gJ100p0ETA25', 'L1_gLJ80p0ETA25', 'L1_gLJ100p0ETA25', 'L1_gLJ140p0ETA25', 'L1_gLJ160p0ETA25', 'L1_SC111-CjJ40', 'L1_SC111-CJ15',  'HLT_j200_0eta180_emergingPTF0p08dR1p2_a10sd_cssk_pf_jes_ftf_preselj200_L1J100', 'HLT_j200_0eta160_emergingPTF0p08dR1p2_a10sd_cssk_pf_jes_ftf_preselj200_L1J100', 'HLT_j200_0eta160_emergingPTF0p08dR1p2_a10sd_cssk_pf_jes_ftf_preselj200_L1jJ160', 'HLT_j200_0eta180_emergingPTF0p08dR1p2_a10sd_cssk_pf_jes_ftf_preselj200_L1jJ160', 'HLT_j180_hitdvjet260_medium_L1J100', 'VetoTrigDec']

alg.triggerStrings = ['L1_J100', 'VetoTrigDec']


alg.jetContainers =['HLT_AntiKt10EMPFlowCSSKSoftDropBeta100Zcut10Jets_jes_ftf', 'HLT_AntiKt10LCTopoTrimmedPtFrac4SmallR20Jets_jes', 'HLT_AntiKt4EMTopoJets_subjesIS',  'HLT_AntiKt4EMTopoJets_subresjesgscIS_ftf', 'HLT_AntiKt4EMTopoJets_nojcalib', 'TruthJets']
# alg.jetContainers =['AntiKt10EMTopoJets', 'TruthJets']

####cutChains MUST belong to handled by the .cxx file, ie.e. 'noCuts', '1p8EtaCut', or 'pT200Cut_1p8EtaCut'.
alg.cutChains =['noCuts', '1p8EtaCut', 'pT200Cut_1p8EtaCut']

#// add trig decicion tool here https://atlassoftwaredocs.web.cern.ch/AnalysisSWTutorial/trig_decisiontool/
#https://atlassoftwaredocs.web.cern.ch/AnalysisSWTutorial/trig_analysis/

#xAODConfTool = createPublicTool( 'TrigConf::xAODConfigTool', 'xAODConfigTool' )
#job.algsAdd (xAODConfTool)
#addPrivateTool( alg, 'trigDecisionTool', 'Trig::TrigDecisionTool' )

#alg.trigDecisionTool.ConfigTool = '%s/%s' % ( xAODConfTool.getType(), xAODConfTool.getName() )
#alg.trigDecisionTool.TrigDecisionKey = "xTrigDecision"

# We need to explicitly instantiate CutFlowSvc in Athena
#from EventBookkeeperTools.CutFlowHelpers import CreateCutFlowSvc
#CreateCutFlowSvc( seq=athAlgSeq )

# Add our algorithm to the main alg sequence
athAlgSeq += alg

# Limit the number of events (for testing purposes)
#theApp.EvtMax = 100000000000

# Optional include for reducing printout from athena
#include("AthAnalysisBaseComps/SuppressLogging.py")

jps.AthenaCommonFlags.HistOutputs = ["ANALYSIS:TrigJetsxAODAnalysis.outputs.root"]
svcMgr.THistSvc.MaxFileSize=-1 #speeds up jobs that output lots of histograms

