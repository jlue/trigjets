from AnaAlgorithm.AlgSequence import AlgSequence
from AnaAlgorithm.DualUseConfig import createService

# Add configurations here

def makeSequence (dataType) :

    algSeq = AlgSequence()

    # Set up CommonServiceSequence
    from AsgAnalysisAlgorithms.CommonServiceSequence import makeCommonServiceSequence
    makeCommonServiceSequence (algSeq, runSystematics = True)


# copied from L1CaloAODAnalysis python L1CaloAODAnalysisConfig.py
# Set up Trigger decision tool if required
#    if hasattr(alg, 'triggerDecTool'):
#        from TrigDecisionTool.TrigDecisionToolConfig import TrigDecisionToolCfg
#        tdt = acc.getPrimaryAndMerge(TrigDecisionToolCfg(flags))
#        alg.triggerDecTool = tdt


   # Add your sequences here

    # below is from the 2022 tutorial.   

    #triggerChains = [
    #        'HLT_e26_lhtight_nod0_ivarloose',
    #        'HLT_j460_a10r_L1J100',
    #        'HLT_j450_L1jJ160',
    #        'HLT_j200_0eta180_emergingPTF0p08dR1p2_a10sd_cssk_pf_jes_ftf_preselj200_L1jJ160'
    #        'HLT_j460_a10t_lcw_jes_L1gLJ140p0ETA25', 
    #        'HLT_noalg_L1gLJ100p0ETA25'
    #    ]

    # Include and set up the trigger analysis sequence:
    from TriggerAnalysisAlgorithms.TriggerAnalysisSequence import makeTriggerAnalysisSequence
    triggerSequence = makeTriggerAnalysisSequence( dataType, triggerChains=triggerChains, noFilter=True )

    # Add the trigger analysis sequence to the job:
    algSeq += triggerSequence


    return algSeq

    # Do not add anything here!!!
