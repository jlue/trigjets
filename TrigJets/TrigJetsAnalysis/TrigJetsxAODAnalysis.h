#ifndef TrigJetsAnalysis_TrigJetsxAODAnalysis_H
#define TrigJetsAnalysis_TrigJetsxAODAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <AsgTools/ToolHandle.h>
#include <TrigDecisionTool/TrigDecisionTool.h>

#include "xAODTruth/TruthEvent.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTruth/TruthVertexContainer.h"

#include "MCTruthClassifier/IMCTruthClassifier.h"



class TrigJetsxAODAnalysis : public EL::AnaAlgorithm
{
public:
  /// This is a standard algorithm constructor
  TrigJetsxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);

  /// These are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

  std::string m_trigDecTool_name{"TDT:JetTriggerEfficiencies"};


private:
  /// Configuration, and any other types of variables go here.
  //
  ToolHandle<Trig::TrigDecisionTool> m_trigDecTool;
  ToolHandle<IMCTruthClassifier> m_TruthClassifier{this, "MCTruthClassifier", "MCTruthClassifier/MCTruthClassifier", "MCTruthClassifier"};



  unsigned long long m_eventNumber = 0; ///< Event number

  std::vector<std::string> p_triggerStrings[50];
  std::vector<std::string> p_jetContainers[50];
  std::vector<std::string> p_cutChains[50];

  ////Choose "" to fill histograms with vars in the selected jet container, and "_Truth" to fill histograms with Truth values:////
  std::vector<std::string> m_truthStrings{"", "_Truth"};
  std::string m_strTruth="_Truth";
  std::string m_strNull="";

  ////Variables to fill histograms with. Supported options are "_pT", "_dEta", "_dPhi", "_dR" : ////
  std::vector<std::string> m_varStrings{"_pT", "_dEta", "_dPhi", "_dR" };
  std::string m_strPT = "_pT";
  std::string m_strEta = "_Eta";
  std::string m_strPhi = "_Phi";
  std::string m_strDEta = "_dEta";
  std::string m_strDPhi = "_dPhi";
  std::string m_strDR = "_dR";

  ////strings for readability////
  std::string m_str_= "_";
  std::string m_strh= "h_";

  ////Strings for building histogram axes and titles////
  std::string m_hTitleSuffix = "; pt [GeV]; counts";
  std::string m_hTruthTitleSuffix1 = "; Leading";
  ////In between these two put truthString+varString
  std::string m_hTruthTitleSuffix2 = "; counts";
  std::string m_hTitlePrefix = "max(pT) of jets passing ";

  unsigned int m_trigDecision = 0; ///< Trigger decision

  /// overall output variables
  // std::string p_TriggerString;
  std::string p_jetContainer;
  std::string hName;
  std::string m_hdEtaName;
  std::string m_hdPhiName;  
  std::string m_hRName;  
  int m_NtriggerStrings = 0;
  int m_NjetContainers = 0;
  int m_NcutChains = 0;
  std::string m_strCuts = "";
  std::string m_str2 = "_LeadJetPt_";

  // // try adding this of list of name
  // // borrowing this from AthMonitorAlgorithm
  // Gaudi::Property<std::string> m_triggerChainString {this,"TriggerChain","","comma separated list of chains"}; ///< Trigger chain string pulled from the job option and parsed into a vector
  // std::vector<std::string> m_vTrigChainNames; ///< Vector of trigger chain names parsed from trigger chain string
  // StatusCode parseList( const std::string& line, std::vector<std::string>& result ) const;
  // void unpackTriggerCategories( std::vector<std::string>& vTrigChainNames ) const;


};

#endif
