# MyAnalysis

setupATLAS

asetup AthAnalysis 24.2.41

cd build

cmake ../source/

make

source x86_64-el9-gcc13-opt/setup.sh

cd ../run

athena MyAnalysis/ATestRun_jobOptions.py --filesInput /path/to/AOD/filename.root

athena --loglevel WARNING MyAnalysis/ATestRun_jobOptions.py

Edit share/ATestRun_jobOptions.py to specify desired triggers and jet Containers. Output will include historgrams of events with leading jets of all eta and with only leading jets that pass |eta|\<1.8.

---
